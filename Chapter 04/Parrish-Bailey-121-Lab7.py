#################################################################
# Lab 7:        Software Sales
# Description:  This program will display a discount based on the
#               user's input of software packages purchased and
#               also display the total cost of the purchase.
# Programmer:   Bailey Parrish
# Date:         October 12th, 2017
#################################################################

## Define main module
def main():
    ## Declare and initialize variables
    packages = 0
    total = 0
    
    print("Input the amount of packages purchased: ")
    packages = int(input())
    if 10 <= packages <=19:
        print("You receive a 20 percent discount!")
        total = format(99 * packages * 0.2, '.2f')
    else:
        if 20 <= packages <= 49:
            print("You receive a 30 percent discount!")
            total = format(99 * packages * 0.3, '.2f')
        else:
            if 50 <= packages <=99:
                print("You receive a 40 percent discount!")
                total = format(99 * packages * 0.4, '.2f')
            else:
                if packages >= 100:
                    print("You recieve a 50 percent discount!")
                    total = format(99 * packages * 0.5, '.2f')
                else:
                    print("You receive no discount.")
                    total = format(99 * packages, '.2f')
    print('The final cost of the packages purchased is: ', total)
main()
