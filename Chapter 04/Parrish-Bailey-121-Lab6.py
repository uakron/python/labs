#################################################################
# Lab 6:        Magic Dates
# Description:  This program will multiply the user's input of
#               month and day and determine if it is equal to the
#               user's input of year.
# Programmer:   Bailey Parrish
# Date:         October 10th, 2017
#################################################################

## Define main module
def main():
    ## Declare and initialize variables
    month = 0
    day = 0
    year = 0
    
    print("Enter the month")
    month = int(input())
    print("Enter the day")
    day = int(input())
    print("Enter the year")
    year = int(input())
    if month * day == year:
        print("That date is magic!")
    else:
        print("That date is not magic.")
main()
