###########################################################################
# Lab 13:       Driver's License Exam
# Description:  This program will store 20 correct answers of an exam into
#               an array and then prompt the user to input their answers.
#               It will then grade the student's answers and display the
#               amount of correct answers and incorrect answers with a list
#               showing the numbers of the incorrect answers.
# Programmer:   Bailey Parrish
# Date:         November 30, 2017
###########################################################################
def main():
    # Declare all variables
    questionNumber = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    correctAnswers = ['B','D','A','A','C','A','B','A','C','D','B','C','D','A','D','C','C','B','D','A']
    studentsAnswers = [0] * 20
    status = [0] * 20
    index = 0
    correctCount = 0
    incorrectCount = 0

    

    for index in range(20):
        # Get the student's answers
        print('Please enter the answer for question number', str(questionNumber[index]) + ': '),
        studentsAnswers[index] = str.upper(input())
        if studentsAnswers[index] == correctAnswers[index]:
            correctCount += 1
            status[index] = "Correct"
        else:
            incorrectCount += 1
            status[index] = "Incorrect"
            
    if 15 <= correctCount <= 20:
            print('The student passed the exam.')
    else:
            print('The student did not pass the exam.')
    
    print('The number of correct answers:', correctCount)
    print('The number of incorrect answers:', incorrectCount)
    print('These are the numbers of the incorrect answers:')
    for index in range(20):
        if status[index] == "Incorrect":
            print(questionNumber[index])

main()
    
    
