############################################################################
# Lab 12:       Rainfall Statistics
# Description:  This program will accept rainfall values for each month then
#               calculate the total, average, and the highest and lowest
#               monthly rainfal values for the whole year.
# Programmer:   Bailey Parrish
# Date:         November 28, 2017
############################################################################
def main():
    # Declare and initialize all variables
    rainfall = [0,0,0,0,0,0,0,0,0,0,0,0]
    months = ['January','February','March','April','May','June','July','August','September','October','November','December']
    index = 0
    total = 0
    average = 0
    lowest = 1000
    highest = 0

    # Get the rainfall for each month
    for index in range(12):
        print('Enter the amount of rain in', months[index] + ': '),
        rainfall[index] = int(input())
        total = total + rainfall[index]
        if rainfall[index] < lowest:
            lowest = rainfall[index]
        if rainfall[index] > highest:
            highest = rainfall[index]

    average = total / len(months)

    showStats(total, average, lowest, highest)

def showStats(total, average, lowest, highest):
    print('The total rainfall is:', total)
    print('The average rainfall is:', average)
    print('The lowest monthly rainfall was:', lowest)
    print('The highest monthly rainfall was:', highest)

    
        
main()
