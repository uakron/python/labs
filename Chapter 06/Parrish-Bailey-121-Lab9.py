##########################################################################
# Lab 9:        Rectangle Area
# Description:  This program prompts the user to enter a rectangle's
#               width and length, and then displays the rectangles's area.
# Programmer:   Bailey Parrish
# Date:         October 31, 2017
##########################################################################
def main():
    # Declare and initialize length, width, and area variables
    length = 0
    width = 0
    area = 0
    
    print("Enter the length of the rectangle:")
    length = float(input())
    print("Enter the width of the rectangle:")
    width = float(input())
    area = areaRectangle(length, width)
    print("The area of the rectangle is:", format(area, '.2f'))

def areaRectangle(length, width):
    area = 0
    area = length * width
    return area

main()


