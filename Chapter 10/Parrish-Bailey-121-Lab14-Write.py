##########################################################################
# Lab 14:       Average of Numbers
# Description:  This is the first of two programs. This program will write
#               5 numbers to a file named numbers.dat.
# Programmer:   Bailey Parrish
# Date:         December 7, 2017
##########################################################################
def main():
    numNumbers = int(input('How many numbers do you want to average? '))

    numbers_file = open('numbers.dat', 'w')

    for count in range(1, numNumbers + 1):
        numbers = int(input('Enter a value for #' + str(count) + ': '))

        numbers_file.write(str(numbers) + '\n')

    numbers_file.close()
    print('Values written to numbers.dat.')

main()
