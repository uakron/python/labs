######################################################################
# Lab 14:       Average of Numbers
# Description:  This is the second part of two programs. This program
#               will read the numbers in the numbers.dat file, average
#               them, and return the value.
# Programmer:   Bailey Parrish
# Date:         December 7, 2017
######################################################################
def main():
    sumNumbers = 0
    length = 0
    average = 0

    numbers_file = open('numbers.dat', 'r')

    for line in numbers_file:
        numbers = float(line)
        sumNumbers += numbers
        length += 1

    average = sumNumbers / length
        
    print('The average of the values is:', format(average, '.2f'))

    numbers_file.close()

main()
