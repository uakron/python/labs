######################################################################
# Lab 8:        Distance Traveled
# Description:  This program will calculate the distance traveled
#               by a vehicle based on the user's input of speed in MPH
#               and time in hours using a for loop.
# Programmer:   Bailey Parrish
# Date:         October 26th, 2017
######################################################################

## Define main module
def main():
    calculateDistance(inputSpeed(), inputTime())

## Define inputSpeed module   
def inputSpeed():
    ## Declare speedMPH variable
    speedMPH = 0
    print("What is the speed of the vehicle in MPH?")
    speedMPH = int(input())
    return speedMPH

## Define inputTime module
def inputTime():
    ## Declare timeHours variable
    timeHours = 0
    print("How many hours has it traveled?")
    timeHours = int(input())
    return timeHours

## Define calculateDistance module
def calculateDistance(speedMPH, timeHours):
    ## Declare distanceTraveled variable
    distanceTraveled = 0
    print("Hour\tDistance Traveled")
    print("-------------------------")
    for timeHours in range(1, timeHours +1, 1):
        distanceTraveled = speedMPH * timeHours
        print(timeHours,'\t', distanceTraveled)

main()
