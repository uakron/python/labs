#####################################################################
## Lab 1: Calculating Miles-per-Gallon
## Description: This program calculates miles-per-gallon based on the
## user's input of miles driven and gallons of gas used.
## Programmer: Bailey Parrish
## Date: September 7, 2017
#####################################################################
def main():
    
    milesDriven = float(input("Enter the number of miles driven: "))
    gallonsGas = float(input("Enter the gallons of gas used: "))
    MPG = milesDriven / gallonsGas
    print('The MPG of the car is', MPG)

main()
