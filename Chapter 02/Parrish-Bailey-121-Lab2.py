###############################################################################
# Lab 2: Converting Celsius to Fahrenheit
# Description: This program converts Celsius temperature to Fahrenheit based on
# the user's input of temperature.
# Programmer: Bailey Parrish
# Date: September 12, 2017
###############################################################################
tempC = int(input("Enter the temperature in degrees Celsius: "))
tempF = int(9/5*(tempC) + 32)
print('The temperature in degrees Fahrenheit is', tempF)
