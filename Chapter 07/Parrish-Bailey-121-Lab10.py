#####################################################################
# Lab 10:       Payroll Program with Input Validation
# Description:  This program will validate the user's input of hourly
#               pay and hours worked.
# Programmer:   Bailey Parrish
# Date:         November 7, 2017
#####################################################################
def main():
    ## Declare and initialize hourlyPay, hoursWorked, and grossPay variables
    hourlyPay = 0
    hoursWorked = 0
    grossPay = 0
    print("Enter your hourly pay:")
    hourlyPay = float(input())
    while hourlyPay < 7.50 or hourlyPay > 18.25:
        print("ERROR: Please enter a value between $7.50 and $18.25")
        hourlyPay = float(input())
    print("Enter your hours worked:")
    hoursWorked = float(input())
    while hoursWorked < 0 or hoursWorked > 40:
        print("ERROR: Please enter a value between 0 and 40")
        hoursWorked = float(input())
    grossPay = hourlyPay * hoursWorked
    print("Your gross pay is ", format(grossPay, '0.2f'))
main()
