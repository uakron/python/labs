############################################################################
# Lab 11:       Speeding Violation Calculator
# Description:  This program calculates and displays the number of miles per
#               hour over the speed limit that a speeding driver was doing.
# Programmer:   Bailey Parrish
# Date:         November 14, 2017
############################################################################
def main():
    # Declare and initialize the speedLimit, driverSpeed, and overLimit variables
    speedLimit = 0
    driverSpeed = 0
    overLimit = 0
    
    print("Enter a speed limit of at least 20MPH but no greater than 70MPH:")
    speedLimit = int(input())
    while speedLimit < 20 or speedLimit > 70:
        print("Please enter a valid speed limit:")
        speedLimit = int(input())
    print("Enter the driver's speed:")
    driverSpeed = int(input())
    while driverSpeed <= speedLimit:
        print("The driver was not speeding. Enter a value over the speed limit:")
        driverSpeed = int(input())
    overLimit = driverSpeed - speedLimit
    print("The MPH over the speed limit is:", overLimit)
main()
