####################################################################
# Lab 5:        Calorie Counter
# Description:  This programs will calculate fat calories and carb
#               calories based on the user's input of fat grams and
#               carb grams.
# Programmer:   Bailey Parrish
# Date:         October 6th, 2017
####################################################################

## Define calculateCarbCalories module
def calculateCarbCalories():
    ## Declare and initialize carbCalories
    carbCalories = 0
    
    carbCalories = carbGrams * 4
    print('The amount of calories from carbs is: ', carbCalories)
    return carbCalories

## Define calculateFatCalories module
def calculateFatCalories():
    ## Declare and initialize fatCalories
    fatCalories = 0
    
    fatCalories = fatGrams * 9
    print('The amount of calories from fat is: ', fatCalories)
    return fatCalories

# Main
## Define fatGrams and carbGrams
fatGrams = 0
carbGrams = 0
    
print("Enter the amound of fat grams: ")
fatGrams = int(input())
print("Enter the amount of carb grams: ")
carbGrams = int(input())
calculateFatCalories()
calculateCarbCalories()
