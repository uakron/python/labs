########################################################################
# Lab 3: Converting Kilometers to Miles
# Description: This program will convert the user's input of distance in
# kilometers to distance in miles.
# Programmer: Bailey Parrish
# Date: September 19, 2017
########################################################################

## Define milesConversion module
def milesConversion():
    ## Declare and initialize variable
    distanceMiles = 0
    
    distanceMiles = int(distanceKilometers * 0.6214)
    print("The distance in miles is", distanceMiles)
    return distanceMiles

## Main
## Declare and initialize variable
distanceKilometers = 0

print("Enter the distance in kilometers")
distanceKilometers = int(input())
milesConversion()
