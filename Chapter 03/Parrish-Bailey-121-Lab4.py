#########################################################################
# Lab 4:        Autombile Costs
# Description:  This program adds up the input of monthly autmobile costs
# Programmer:   Bailey Parrish
# Date:         September 26, 2017
#########################################################################

# Declare and Initialize Global Variables
gasExpense = 0
insuranceExpense = 0
loanExpense = 0
maintenanceExpense = 0
oilExpense = 0
tiresExpense = 0
totalMonthly = 0
totalAnnual = 0

# Define Modules
def inputGas():
    global gasExpense
    print("Enter the monthly gas expense")
    gasExpense = float(input())

def inputInsurance():
    global insuranceExpense
    print("Enter the monthly insurance expense")
    insuranceExpense = float(input())

def inputLoan():
    global loanExpense
    print("Enter the monthly loan expense")
    loanExpense = float(input())

def inputMaintenance():
    global maintenanceExpense
    print("Enter the monthly maintenance expense")
    maintenanceExpense = float(input())

def inputOil():
    global oilExpense
    print("Enter the monthly oil expense")
    oilExpense = float(input())

def inputTires():
    global tiresExpense
    print("Enter the monthly tires expense")
    tiresExpense = float(input())

# Main
inputLoan()
inputInsurance()
inputGas()
inputOil()
inputTires()
inputMaintenance()
totalMonthly = loanExpense + insuranceExpense + gasExpense + oilExpense + tiresExpense + maintenanceExpense
print('Your monthly expenses total: ', totalMonthly)
totalAnnual = totalMonthly * 12
print('Your annual expenses total: ', totalAnnual)
